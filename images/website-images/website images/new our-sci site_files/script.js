jQuery(function(){

AOS.init({
	once: true,
});

jQuery('#nav-icon4').click(function(){
  jQuery(this).toggleClass('open');
  jQuery('.mainMenu').toggleClass('activeMenu');
});

jQuery('.banSlider').slick({
  dots: true,
  arrows: false,
  infinite: true,
  speed: 300,
  autoplay: true,
  autoplaySpeed: 5000,
  adaptiveHeight: true,
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        infinite: true,
        dots: true,
      }
    },
  ]
  
  
});


// Smooth Scroll 
	jQuery(function() {
		jQuery('.smoothClick, .smoothLink > a').click(function() {
		
		var dis = jQuery(this);

		if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
		  var target = jQuery(this.hash);
		  target = target.length ? target : jQuery('[name=' + this.hash.slice(1) +']');
		  if (target.length) {
			$('html, body').animate({
			  scrollTop: target.offset().top-80
			}, 1000);
			return false;
		  }
		}
	  });
	});

  /*jQuery(".questAns:first-child" ).addClass('active');*/
  jQuery(".faq .quest").click(function() {
    var dis = jQuery(this);
    var quest = dis.parent();
    
    if (quest.hasClass('active')){
      quest.find('.ans').slideUp();
      quest.removeClass('active');
    } else {
      quest.siblings().removeClass('active').find('.ans').slideUp();
      quest.find('.ans').slideDown();
      quest.addClass('active'); 
    }
  });
  
  

if (window.matchMedia("(max-width: 767px)").matches) {
  /* the viewport is less than 768 pixels wide */
  jQuery('.xs-slider').slick({
    dots: false,
    infinite: false,
    arrows: true,
    dots: false,
  });



$('.footer h4').click(function(){
  $(this).parents('.col-12').find('p').slideToggle();
  $(this).toggleClass('active')

});
$('.footer h4').click(function(){
  $(this).parents('.fotLinks').find('ul').slideToggle();
  $(this).toggleClass('active')

});
$('.footer h4').click(function(){
  $(this).parents('.getTouch').find('.botForm').slideToggle();
  $(this).toggleClass('active')
});



} 



}); //---Main Function Close